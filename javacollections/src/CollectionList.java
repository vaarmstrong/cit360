import java.util.*;

public class CollectionList {

    public static void main(String[] args){

        System.out.println("__List__");

        ArrayList list = new ArrayList();

        list.add("Horse");
        list.add("Shark");
        list.add("Mouse");
        list.add("Octopus");
        list.add("Giraffe");
        list.add("Rhino");
        list.add("Llama");

        for (Object str : list) {
            System.out.println((String) str);
        }

        System.out.println("\n");
        System.out.println("__Tree Set__");
        Set set = new TreeSet();
        set.add("Horse");
        set.add("Shark");
        set.add("Mouse");
        set.add("Octopus");
        set.add("Rhino");
        set.add("Llama");
        set.add("Giraffe");

        for (Object str : set) {
            System.out.println((String) str);
        }

        System.out.println("\n");
        System.out.println("__Queue__");
        Queue queue = new PriorityQueue();
        queue.add("Horse");
        queue.add("Shark");
        queue.add("Mouse");
        queue.add("Giraffe");
        queue.add("Octopus");
        queue.add("Rhino");
        queue.add("Llama");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("\n");
        System.out.println("__Set__");
        HashSet hashSet = new HashSet();
        hashSet.add("Horse");
        hashSet.add("Giraffe");
        hashSet.add("Shark");
        hashSet.add("Mouse");
        hashSet.add("Octopus");
        hashSet.add("Rhino");
        hashSet.add("Llama");

        for (Object str : hashSet) {
            System.out.println((String) str);
        }


        System.out.println("\n");
        System.out.println("__Book List__");
        List<Books> bList = new LinkedList<Books>();
        bList.add(new Books("Les Miserable", "Victor Hugo"));
        bList.add(new Books("Fablehaven - Rise of the Evening Star", "Brandon Mull"));
        bList.add(new Books("Pride and Prejudice", "Jane Austen"));
        bList.add(new Books("The Alchemist: The Secrets of the Immortal Nicholas Flamel", "Michael Scott"));
        bList.add(new Books("Fablehaven", "Brandon Mull"));
        bList.add(new Books("The Lightning Thief", "Rick Riordan"));
        bList.add(new Books("Belgarath the Sorcerer", "David and Leigh Eddings"));


        for (Books book : bList) {
            System.out.println(book);
        }
    }
}
